﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Homework
{
    public partial class MainPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// テキストボックスの入力値のチェックをして
        /// ラベルにメッセージを設定します。
        /// </summary>
        /// <param name="sender">イベント発生元</param>
        /// <param name="e">イベント付加情報</param>
        protected void TextCheckButton_Click(object sender, EventArgs e)
        {
            string errorMessage = string.Empty;
            bool errorFlag = false;

            Label.ForeColor = Color.Red;

            // 空文字チェック
            if (TextBox.Text == string.Empty)
            {
                Label.Text = "文字を入力してください。";
                return;
            }

            // 文字数チェック
            if (TextBox.Text.Length > 10)
            {
                // 10文字を超える場合
                errorMessage += "10文字を超えています。";
                errorFlag = true;
            }
            else
            {
                // 処理なし
            }

            // フォーマットチェック
            if (!new Regex("^[0-9a-zA-Z]+$").IsMatch(TextBox.Text))
            {
                // 英数字以外が含まれる場合
                errorMessage += "英数字以外が含まれています。";
                errorFlag = true;
            }
            else
            {
                // 処理なし
            }

            // 
            if (errorFlag)
            {
                Label.Text = errorMessage;
            }
            else
            {
                Label.ForeColor = Color.Black;
                Label.Text = "入力OK";
            }
        }
    }
}